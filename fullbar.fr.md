---
title: "Conditions de service"
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: tos
text_align: left
---

<br>
**v1.2.2 - Mai 2024**


## A propos de ce document
Ce document a été rédigé à l'origine en anglais et est la seule version dont **Stichting Disroot.org** peut être tenu pour responsable.<br>
Toute traduction de ces **Conditions de service** est un effort communautaire pour rendre l'information accessible dans d'autres langues et doit être considérée comme telle, sans autre valeur que celle d'une simple information.

## Acceptation de ces conditions
En utilisant l'un des services fournis par **Disroot.org**, vous acceptez être soumis par nos conditions de service. Vous devez donc commencer à lire ces conditions avant d'utiliser notre plate-forme.

## Note de la communauté
**Disroot** est une communauté et notre plateforme fait partie de l'écosystème du logiciel libre et de l'open source. Nous partageons tous les ressources des serveurs, collaborons et utilisons ensemble les services de **Disroot.org**. Il est très important que chacun d'entre nous le comprenne et s'en sente également responsable. Veuillez donc respecter les directives suivantes afin que nous nous entendions tous.

--- 

<a id="table-of-contents"></a>
# Table des matières

## [Conditions d'utilisation](#terms-of-services)<br>
[1. Qui peut utiliser nos services?](#who)<br>
[2. Limites d'espace sur le disque](#space-limits)<br>
[3. Expiration du compte](#expiration)<br>
[4. Mot de passe](#password)<br>
[5. Pourriels et virus](#spam)<br>
[6. Droits d'auteur](#copyright)<br>
[7. Activités non autorisées](#activities)<br>
[8. Utiliser les services de Disroot pour des activités commerciales](#commercial-activities)<br>
[9. Propriété et responsabilité du contenu](#ownership)<br>
[10. Responsabilités juridiques](#legal)<br>
[11. Lois](#laws)<br>
[12. Résiliation du compte](#account-termination)<br>
[13. Pas de garantie](#warranty)<br>

## [Principes de base de la Communauté en matière de coexistence](#community-guidelines)<br>
[1. Respecter si l'on veut être respecté](#respect)<br>
[2. Ne pas contribuer aux abus](#abuse)<br>
[3. Ne pas promouvoir l'intolérance et la violence](#violence)<br>
[4. Prendre soin de notre espace](#our-space)<br>

## [Modifications des présentes conditions](#changes)
<br>

---

<a id="terms-of-services"></a>
# Conditions d'utilisation des services

<a id="who"></a> 
## 1. Qui peut utiliser nos services ?
Toute personne âgée d'au moins 16 ans peut utiliser les services.

<a id="space-limits"></a> 
## 2. Limites d'espace sur le disque
**Les ressources sont rares**. N'oubliez pas que la conservation de fichiers et de courriers électroniques inutiles peut limiter l'espace sur le serveur, ce qui peut entraîner une diminution du nombre de comptes accessibles aux autres.<br>**Ne gaspillez pas!**

<a id="expiration"></a> 
## 3. Expiration du compte
Pour des raisons de sécurité, **Disroot.org** n'expire pas les comptes inutilisés. Cela peut être sujet à changement si nous sommes obligés d'économiser de l'espace disque pour d'autres utilisateurs. De telles modifications de nos conditions seront annoncées par tous les canaux de communication bien à l'avance. Nous préférons que les personnes prennent la responsabilité de leur compte et de son impact sur les ressources de **Disroot**, et plutôt [**détruisent leur compte**](https://user.disroot.org) lorsqu'elles décident de ne plus utiliser nos services, ou de purger les fichiers inutilisés.

<a id="password"></a> 
## 4. Mot de passe
Nous ne gardons jamais trace de quiconque demande l'activation d'un compte **Disroot**. Lors de la création de votre compte, vous pouvez soit choisir des questions de sécurité et/ou ajouter un e-mail secondaire, qui vous permettront tous deux de récupérer votre mot de passe au cas où vous le perdriez/oublieriez.<br>
- Si vous ne définissez pas d'e-mail secondaire ou si vous oubliez la question ou les réponses aux questions, nous ne pourrons pas vous renvoyer les données pour accéder à nouveau à votre compte. N'oubliez pas non plus de changer régulièrement votre mot de passe : vous pouvez le faire en vous connectant au Libre-service utilisateur (https://user.disroot.org) et en cliquant sur le bouton "Changer le mot de passe".

**! ATTENTION !** Les mots de passe perdus entraînent la perte irrémédiable des fichiers du Cloud. Pour vous en protéger, vous pouvez activer la fonction de récupération des mots de passe dans vos paramètres personnels du Cloud.

<a id="spam"></a> 
## 5. Pourriels et virus
Pour éviter que nos serveurs soient inclus dans des listes noires :
- Nous n'autorisons pas l'utilisation des comptes de messagerie Disroot pour envoyer du pourriel. Tout compte surpris en train de le faire sera désactivé sans autre avertissement.

- Si vous recevez une grande quantité de pourriels ou de courriels suspects, veuillez nous en informer à l'adresse support@disroot.org afin que nous puissions enquêter plus avant.

<a id="copyright"></a> 
## 6. Droits d'auteur
L'un des principaux objectifs de **Disroot** est de promouvoir les logiciels libres et open source ainsi que tout type de contenu laissé libre. Adoptez-le et soutenez les artistes et les projets qui publient leur travail sous ces licences.
- Télécharger et (surtout) partager publiquement du matériel sous droit d'auteur sur **Disroot** pourrait mettre d'autres utilisateurs et **Disroot.org** dans de sérieux problèmes légaux ainsi que mettre en danger l'ensemble du projet et l'existence de nos serveurs (et tous les services qui y sont connectés) alors **ne le faites pas**.

<a id="activities"></a> 
## 7. Activités non autorisées
Ne vous adonnez pas aux activités suivantes par le biais des services fournis par **Disroot.org** :

<ol class=disc>
<li><b>Harceler et abuser les autres</b> en proférant des menaces, en les traquant ou en leur envoyant du pourriel.</li>
<li><b>Contribuer à la discrimination, au harcèlement ou au préjudice contre tout individu ou groupe.</b> Cela inclut la propagation de la haine et du sectarisme par le racisme, l'ethnophobie, l'antisémitisme, le sexisme, l'homophobie et toute autre forme de comportement discriminatoire.</li>
<li><b>Contribuer à la maltraitance d'autrui</b> en distribuant du matériel dont le processus de production a engendré des violences ou des agressions sexuelles contre des personnes ou des animaux.</li>
<li><b> Usurper l'identité d'une autre personne ou la présenter de manière confuse ou trompeuse, à moins que le récit ne soit clairement classé comme une parodie.</li>
<li><b>Doxing</b> : la publication des données personnelles d'autres personnes n'est pas autorisée parce que:<br> (I) nous ne pouvons évaluer ni les buts ni les conséquences qu'elle aura pour les personnes exposées, et (II), elle implique un risque juridique trop important pour <b>Disroot.org</b>.</li>
<li><b>Mauvaise utilisation des services</b> en diffusant des virus ou des logiciels malveillants, en se livrant à une attaque par déni de service ou en tentant d'obtenir un accès non autorisé à tout système informatique, y compris celui-ci.</li>
<li><b>Activités non autorisées</b> par le droit national ou international.</li>
</ol>

<a id="commercial-activities"></a> 
## 8. Utiliser les services de Disroot pour des activités commerciales
**Disroot** est une organisation à but non lucratif qui fournit des services aux particuliers sur la base du "paiement à la carte". En raison de cette structure, nous considérons l'utilisation des services de **Disroot** à des fins commerciales comme un abus de service et elle sera traitée comme telle.
Ces activités ou objectifs commerciaux incluent, mais ne sont pas limités à :
<ol class=disc>
<li>Effectuer des échanges de services <b>Disroot</b> avec un service tiers.<br>C'est interdit.</li>
<li> Utilisation du courrier électronique à des fins telles que le type de compte "sans réponse" pour une entreprise.<br>Si une telle activité est détectée, le compte sera bloqué sans avertissement.</li>
<li> Envoi de courriers électroniques en nombre, y compris, mais sans s'y limiter, le marketing et la publicité, à des fins commerciales.<br>
Le compte sera traité comme un pourriel et bloqué dès qu'il sera découvert, sans avertissement préalable.</li>
<li>Les comptes utilisés principalement à des fins de gain financier, y compris, mais sans s'y limiter, le commerce ou la gestion des ventes, ne sont pas tolérés.<br>Ces comptes feront l'objet d'une résiliation sur enquête.</li>
</ol>
<br>
Bien que nous soyons pas favorables à l'utilisation de <b>Disroot</b> pour des activités commerciales, nous comprenons également que certaines activités financières peuvent faire partie de votre vie personnelle, de vos hobbies et de vos passions.
<br>
En règle générale, vous devez nous contacter si votre utilisation commerciale dépasse votre utilisation personnelle, ou si vous avez des doutes et des questions sur la portée du terme "activités commerciales".
<br>
L'utilisation des services <b>Disroot</b> pour d'autres activités commerciales sera examinée au cas par cas et la décision de résilier ces comptes sera basée sur la communication avec le titulaire du compte et le type d'activités en question.
<br>
Si vous avez des doutes ou des questions sur la portée du terme "activités commerciales", veuillez nous contacter.

<a id="ownership"></a> 
## 9. Propriété et responsabilité du contenu
**Disroot.org** n'est pas responsable du contenu publié sur l'un de nos services. Vous êtes responsable de votre utilisation des services de **Disroot**, de tout contenu que vous fournissez et de toutes les conséquences qui en découlent. Toutefois, pour garantir notre existence, nous nous réservons le droit de supprimer tout contenu s'il s'avère être en violation avec la loi applicable.

<a id="legal"></a> 
## 10. Responsabilités juridiques
Comme pour tout ce que vous faites, sachez que **Disroot.org** n'est pas responsable de ce que vous écrivez, ni de la sauvegarde de votre propre vie privée. Nous vous invitons donc à profiter de tous les outils existants pour défendre vos droits.<br>
Pour en savoir plus sur la manière dont vos informations sont stockées et utilisées, veuillez vous référer à notre **Déclaration de confidentialité**.

<a id="laws"></a> 
## 11. Lois
**Disroot.org** est hébergé aux **Pays-Bas** et est donc soumis aux lois et à la juridiction **hollandaises**.

<a id="termination"></a> 
## 12. Résiliation du compte
**Disroot.org** peut mettre fin à votre service à tout moment dans les conditions suivantes :
<ol class=disc>
<li>Il a été constaté que le compte envoyait du pourriel (quantité excessive de courriels non sollicités).</li>
<li>Le compte s'est engagé dans une ou plusieurs des activités interdites énumérées ci-dessus.</li>
</ol>
Les conditions ci-dessus sont susceptibles d'être modifiées à tout moment.

<a id="warranty"></a>
## 13. Pas de garantie
Vous comprenez et acceptez que **Disroot.org** vous fournisse principalement des services basés sur Internet. Par conséquent, les services sont offerts tels quels, sous réserve de leur disponibilité et sans responsabilité à votre égard. Bien que nous ne souhaitions pas décevoir nos utilisateurs, nous ne pouvons donner aucune garantie quant à la fiabilité, l'accessibilité ou la qualité de nos services.<br> Vous acceptez que l'utilisation de nos services est à vos risques et périls.
<br>

<a id="community-guidelines"></a>
# Principes de base de la Communauté en matière de coexistence
**Disroot** est une communauté, c'est-à-dire un groupe de personnes qui partagent des choses en commun. Et la chose la plus commune que nous partageons, outre le fait d'être des utilisateurs de la plateforme, est que nous sommes tous différents et que nous avons donc des façons particulières de penser, de ressentir et de créer des liens.  

Nos conditions d'utilisation régissent de manière générale tous les services. Cependant, dans ceux qui sont conçus pour que les gens interagissent, il est également nécessaire de garantir l'exercice de nos différences dans des conditions de respect et de sécurité afin que nous puissions tous nous exprimer sans craindre de mauvais traitements de quelque nature que ce soit.

Pour tenter d'y parvenir, nous avons établi une série de conditions de base pour la coexistence. Ces conditions n'ont pas vocation à être parfaites et monolithiques, elles peuvent donc évoluer dans le bon sens. Si nécessaire, la communauté en sera dûment informée.  

Si vous estimez que l'une de ces conditions contredit ou ne correspond pas à vos principes ou limite d'une manière ou d'une autre votre façon d'être, vous pouvez choisir de ne pas l'utiliser nos instances ou de'en chercher une autre où vous vous sentirez moins conditionné.

<a id=« respect »></a>
## 1. Respecter si l'on veut être respecté
Les insultes, les commentaires et les discours disqualifiants à l'égard d'une personne ou d'un groupe en raison de leur sexe, de leur condition sociale, raciale, physique, mentale ou idéologique sont considérés comme des **comportements violents** et ne sont pas tolérés. Cette catégorie comprend également les **menaces** et le **harcèlement**.

<a id="abuse"></a>
## 2. Ne pas contribuer aux abus
La distribution de matériel dont le processus de production a généré de la violence, des agressions sexuelles et l'exploitation d'adultes ou de mineurs n'est pas admissible dans notre communauté.

<a id="violence"></a>
## 3. Ne pas promouvoir l'intolérance et la violence
Le fascisme, le nazisme et toute autre expression politique ou religieuse qui encourage la violence contre des groupes de personnes n'ont pas leur place dans cette instance.  

Si un membre de la communauté ne respecte pas ou viole intentionnellement l'une des lignes directrices susmentionnées, son compte peut être bloqué ou même supprimé sans préavis et immédiatement. 

<a id="our-space"></a>
## 4. Prendre soin de notre espace
La nature ouverte et diversifiée de Fediverse offre de nombreuses possibilités d'interaction avec des personnes très différentes. Elle augmente également les risques de rencontrer des bots ou des personnes avec lesquelles il est difficile d'interagir. C'est pourquoi chaque membre de la communauté dispose d'outils de modération pour prendre soin de lui-même et des autres.  

Voici les endroits où vous pouvez trouver des outils de modération pour nos services sociaux : 

[FEDisroot](https://howto.disroot.org/en/tutorials/social/fedisroot/interacting#interacting-options)  

[D·Scribe](https://howto.disroot.org/en/tutorials/social/dscribe/communities#moderation)

[Movim](https://howto.disroot.org/en/tutorials/chat/webchat#moderation)  
  

Il existe deux niveaux de modération.

Chaque utilisateur est responsable du blocage et/ou de la mise en sourdine des comptes avec lesquels il ne souhaite pas interagir. Si le compte provient d'une instance externe, vous pouvez également envoyer un rapport aux administrateurs de cette instance.  

Signaler un autre utilisateur de la même instance est une ressource à utiliser lorsque quelqu'un viole l'une des **Conditions d'utilisation**, les directives de base mentionnées ci-dessus ou lorsque les outils ne sont pas suffisamment efficaces dans un cas particulier.  

Les administrateurs peuvent intervenir à tout moment si un cas le justifie et prendre les mesures appropriées.

Le respect et la protection de notre communauté est une responsabilité collective de tous ses membres.

<br>
<a id="changes"></a>
## Modifications des présentes conditions
## 14. Modifications de ces conditions
Nous nous réservons le droit de modifier ces conditions à tout moment. Nous attachons une grande importance à la transparence et faisons de notre mieux pour que les utilisateurs soient informés dès que possible de tout changement majeur par courrier électronique, par les réseaux sociaux et/ou par une annonce sur notre [blog] (https://disroot.org/fr/blog). Les modifications mineures seront très probablement publiées uniquement sur notre blog.
<br>
Vous pouvez suivre l'historique des changements sur ce document sur notre dépôt git [**ici**](https://git.disroot.org/Disroot/Disroot-ToS/commits/branch/main)ou le  [**journal des modifications**](https://git.disroot.org/Disroot/Disroot-ToS/src/branch/main/changelog)


[Retour au début](#top)
