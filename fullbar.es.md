---
title: 'Condiciones de Servicio'
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: tos
text_align: left
---

<br>
**v1.2.2 - Mayo 2024**


## Sobre este documento
Este documento ha sido escrito originalmente en Inglés y es el único por el cual **Stichting Disroot.org** puede ser responsable.<br>
Cualquier traducción de estas **Condiciones de Servicio** es un esfuerzo de la comunidad para hacer accesible la información en otros idiomas y debe tomarse como tal, sin ningún otro valor que el meramente informativo.

# Aceptación de estas Condiciones
Al utilizar cualquiera de los servicios provistos por **Disroot.org** estás de acuerdo con las obligaciones que estas Condiciones establecen. Así que deberías empezar por leerlas primero antes de usar nuestra plataforma.

## Aviso a la Comunidad
**Disroot** es una comunidad y nuestra plataforma es parte del ecosistema de Software Libre y de Código Abierto. Es importante que todas y cada una de las personas que utilizamos sus servicios, compartimos los recursos de sus servidores y colaboramos juntas, lo comprenda y se sienta igualmente responsable por esto. Así que, por favor, respetemos los siguientes lineamientos así podemos llevarnos bien.

---

<a id="indice"></a>
# Índice

## [Condiciones de Servicio](#condiciones)<br>
[1. ¿Quién puede utillizar nuestros servicios??](#quien)<br>
[2. 2. Límites de espacio en el disco](#espacio)<br>
[3. Caducidad de la cuenta](#caducidad)<br>
[4. Contraseña](#password)<br>
[5. SPAM y Virus](#spam)<br>
[6. Material protegido por Derechos de Autor](#copyright)<br>
[7. Actividades no permitidas](#activities)<br>
[8. Utilizar los servicios de Disroot para actividades comerciales](#actividades-comerciales)<br>
[9. Autoría de, y responsabilidad por, los contenidos](#autoria)<br>
[10. Responsabilidades legales](#legal)<br>
[11. Leyes](#leyes)<br>
[12. Cancelación de la cuenta](#cancelacion)<br>
[13. Sin Garantía](#warranty)<br>

## [Pautas Básicas de Convivencia de la Comunidad](#pautas)<br>
[1. Respetemos si queremos ser respetados](#respeto)<br>
[2. No contribuyamos al abuso](#abuso)<br>
[3. No promovamos intolerancia y la violencia](#violencia)<br>
[4. Cuidando nuestro espacio](#nuestro-espacio)<br>

## [Cambios en estas Condiciones](#cambios)

---

<a id="condiciones"></a>
# Condiciones de Servicio

<a id="quien"></a>
## 1. ¿Quién puede utillizar nuestros servicios?
Cualquier persona que tenga por lo menos 16 años de edad puede utilizar los Servicios.

<a id="espacio"></a>
## 2. Límites de espacio en el disco
**Los recursos son escasos**. Tengamos en cuenta que mantener archivos y correos innecesarios puede limitar el espacio en el servidor y generar que haya menos cuentas para servir a otras personas.<br>**¡No desperdiciemos!**

<a id="caducidad"></a>
## 3. Caducidad de la cuenta
Por razones de seguridad, **Disroot.org** no caduca las cuentas sin uso. Esto puede estar sujeto a modificaciones si nos vemos forzados a guardar espacio en el disco para otras personas usuarias. Tales modificaciones en nuestras condiciones serán anunciadas a través de todos nuestros canales de comunicación con anticipación.  
Apreciamos que la gente se haga responsable por su cuenta y su impacto en los recursos de **Disroot** y la [cancele](https://user.disroot.org) cuando decida no seguir utilizando nuestros servicios o recuerde purguar los archivos que ya no usa.

<a id="password"></a>
## 4. Contraseña
Nunca mantenemos registros de quienes solicitan la activación de una cuenta de **Disroot**. Cuando creas una cuenta puedes elegir establecer preguntas de seguridad y/o agregar un correo secundario, ambas opciones te permitirán recuperar tu contraseña en caso que la pierdas/olvides.  
- Si no estableces un correo secundario u olvidas las preguntas, o sus respuestas, no podremos reenviarte la información para acceder nuevamente a tu cuenta. También recuerda cambiar tu contraseña periódicamente: puedes hacerlo iniciando sesión en el [Centro de Auto-gestión de la cuenta](https://user.disroot.org) y hacer clic en el botón 'Cambiar contraseña'.  

**¡ AVISO !** El olvido/extravío de la contraseña producirá la pérdida irrecuperable de los archivos en la nube. Para protegerte de ello, puedes habilitar la opción "recuperación de contraseña" en tus configuraciones personales de la Nube.

<a id="spam"></a>
## 5. SPAM y Virus
Para evitar que nuestros servidores sean incluidos en listas negras:  
- No permitimos el uso de las cuentas de correo de Disroot para enviar SPAM. Cualquier cuenta que fuera encontrada haciéndolo será deshabilitada sin previo aviso.

- Si sucediera que recibes una gran cantidad de SPAM o cualquier correo sospechoso, por favor, notifícanos a support@disroot.org así podemos investigarlo.

<a id="copyright"></a>
## 6. Material protegido por Derechos de Autor
Uno de los principales objetivos de **Disroot** es promover programas Libres y de Código Abierto y toda clase de contenido [copyleft](https://es.wikipedia.org/wiki/Copyleft). Abrazálo y apoya a artistas y proyectos que liberan sus trabajos bajo esas licencias.
- Subir y (sobre todo) compartir públicamente material protegido por Derechos de Autor en **Disroot** podría poner a otras personas y a **Disroot** mismo en serios problemas legales, haciendo peligrar al proyecto entero y la existencia de nuestros servidores (y todos los servicios conectados a ellos) así que **no lo hagas**.

<a id="not_allowed"></a>
## 7. Actividades no permitidas
No te involucres en las siguientes actividades a través de los servicios provistos por **Disroot.org**:

<ol class=disc>
<li> <b>Acosar y abusar de otras personas</b> mediante amenazas, acoso o envío de correo basura.
</li>
<li> <b>Contribuir a la discriminación, acoso o daño contra cualquier persona o grupo.</b> Esto incluye la propagación del odio y la intolerancia a través del racismo, la etnofobia, el antisemitismo, el sexismo, la homofobia y cualquier otra forma de comportamiento discriminatorio.</li>
<li> <b>Contribuir al abuso de otras personas</b> mediante la distribución de material cuyo proceso de producción haya generado violencia o agresión sexual contra personas o animales.</li>
<li> <b>Hacerse pasar por otra persona o representarla</b> de manera confusa o engañosa, a menos que la cuenta esté claramente clasificada como <b>parodia.</b></li>
<li> <b>Doxing</b>: la publicación de datos personales de otras personas no está permitida porque:<br>
(I) no podemos evaluar ni los fines ni las consecuencias que tendrá para las personas expuestas, y (II), implica un riesgo legal demasiado grande para <b>Disroot.org</b>.</li>
<li> <b>Uso indebido de los servicios</b> mediante la distribución de virus o programas maliciosos, la participación en un ataque de denegación de servicio o el intento de obtener acceso no autorizado a cualquier sistema informático, incluido este.</li>
<li> <b>Actividades no permitidas</b> por la legislación nacional o internacional.</li>
</ol>

<a id="commercial"></a>
## 8. Utilizar los servicios de Disroot para actividades comerciales
**Disroot** es una organización sin fines de lucro que provee servicios con la modalidad  "paga lo que quieras". Es por esta estructura que vemos la utilización de los servicios de **Disroot** para fines comerciales como un abuso de ellos y será tratado como tal.  
Estas actividades comerciales incluyen, pero no se limitan a:
<ol class=disc>
<li>Comercializar cualquier servicio de <b>Disroot</b> con terceros.<br>Esto está prohibido.</li>
<li>Utilizar el correo electrónico para fines como cuentas del tipo "no-responder" para una empresa.<br> Si tal actividad es detectada, la cuenta será bloqueada sin aviso</li>
<li>Enviar correos masivos, incluyendo, pero  no limitado a, aquellos para hacer marketing  y publicidad con fines de negocio.<br>
La cuenta será tratada como SPAM y bloqueda en cuanto sea descubierta sin previo aviso.</li>
<li>Cuentas utilizadas principalmente para generar beneficios económicos, incluyendo, pero no limitado a, comercialización o gestión de ventas, no serán toleradas.<br> Esas cuentas estarán sujetas a cancelación previa comprobación.</li>
</ol>
<br>
Así como desalentamos el uso de <b>Disroot</b> con fines comerciales, también entendemos que algunas actividades financieras son parte de la vida personal, hobbies y pasiones de cualquiera.
<br>
Por regla general, debes contactarnos si el uso comercial que le das es mayor que el personal, o si tienes cualquier duda o pregunta acerca de los alcances del término "actividades comerciales".
<br>
Utilizar los servicios de <b>Disroot</b> para otras actividades comerciales será examinado por caso y la decisión de cancelar dichas cuentas se basará en la comunicación con la persona titular de la cuenta y las actividades en cuestión.
<br>
Como mencionamos, ante cualquier duda respecto de la amplitud del término "actividades comerciales", por favor, contacta con nosotros.

<a id="ownership"></a>
## 9. Autoría de, y responsabilidad por, los contenidos
**Disroot.org** no es reponsable por ningún contenido publicado en cualquiera de sus servicios. Tú eres responsable del uso que haces de los servicios de **Disroot**, por cualquier contenido que proporciones y por cualquiera de sus consecuencias. Sin embargo, para asegurar nuestra existencia, nos reservamos el derecho de quitar cualquier contenido si viola la legislación aplicable.

<a id="legal"></a>
## 10. Responsabilidades legales
Por cualquier otra cosa que hagas, eres consciente que **Disroot.org** no es responsable de lo que escribes ni por la salvaguardia de tu propia privacidad. Por eso te invitamos a aprovechar todas las herramientas existentes para defender tus derechos.<br><br>
Para conocer más acerca de cómo es almacenada y utilizada tu información, por favor, lee nuestra **Declaración de Privacidad**.

<a id="laws"></a>
## 11. Leyes
**Disroot.org** está ubicado en **Países Bajos** y por lo tanto sujeto a las leyes y jurisdicción **Holandesa**.

<a id="cancelacion"></a>
## 12. Cancelación de la cuenta
**Disroot.org** podría cancelar tu servicio en cualquier momento bajo las siguiente condiciones:
<ol class=disc>
<li>La cuenta ha sido encontrada enviando SPAM (excesiva cantidad de correos no solicitados).</li>
<li>La cuenta se ha involucrado en una o más de las actividades prohibidas listadas arriba.</li>
</ol>
Las condiciones mencionadas están sujetas a modificaciones en cualquier momento.

<a id="warranty"></a>
## 13. Sin Garantía
Comprendes y estás de acuerdo con que **Disroot.org** te brinda principalmente servicios que funcionan sobre internet. Por lo tanto, los servicios se ofrecen tal cual, sujetos a disponibilidad y sin responsabilidad contigo. Aunque no nos gustaría defraudar a ninguna de las personas usuarias, no podemos dar ninguna garantía sobre la fiabilidad, accesibilidad o la calidad de nuestros servicios<br> Estás de acuerdo en que el uso de nuestros servicios es bajo tu único y exclusivo riesgo.

<a id="pautas"></a>
# Pautas Básicas de Convivencia de la Comunidad
**Disroot** es una comunidad, esto es, un conjunto de personas que comparten cosas en común. Y lo más común que compartimos, además de ser personas usuarias de la plataforma, es que todes somos diferentes y por lo tanto tenemos formas particulares de pensar, de sentir y de vincularnos.

Nuestras condiciones de uso gobiernan de manera general al conjunto de los servicios. Pero en aquellos que están diseñados para que las personas interactuen, se hace necesario, además, garantizar el ejercicio de nuestras diferencias en condiciones de respeto y seguridad de manera que podamos expresarnos sin temor a maltratos de ninguna clase.

Para intentar lograr eso, establecimos una serie de condiciones básicas de convivencia. Estas condiciones no pretenden ser perfectas y monolíticas, por lo que pueden cambiar para mejorar. Cuando eso fuera necesario, se informará debidamente a la comunidad.

En caso que consideres que alguna de estas condiciones contradice o no se ajusta a tus principios o limita de alguna manera tu forma de ser, puedes elegir no utilizar nuestras instancias o buscar otra donde te sientas menos condicionado.

<a id="respeto"></a>
# 1. Respetemos si queremos ser respetados
Los insultos, comentarios y discursos descalificadores a otra persona o grupo por su género, condición social, racial, física, mental o ideológica son considerados como **comportamientos violentos** y no son tolerados. En esta categoría también se incluyen **las amenazas** y **el acoso**.

<a id="abuso"></a>
# 2. No contribuyamos al abuso
La distribución de material cuyo proceso de producción haya generado violencia, agresión sexual y explotación de personas adultas o menores de edad no es admisible en nuestra comunidad.

<a id="violencia"></a>
# 3. No promovamos la intolerancia y la violencia
El fascismo, el nazismo y toda otra expresión política o religiosa que promuevan la violencia contra grupos de personas no tienen espacio en nuestras instancias.

En el caso de que algún integrante de la comunidad no respete o intencionalmente viole alguna de las pautas anteriores, su cuenta podría ser bloqueada o incluso eliminada sin previo aviso e inmediatamente.

<a id="nuestro-espacio"></a>
# Cuidando nuestro espacio
La naturaleza abierta y diversa del Fediverso posibilita muchas formas de interacción con muchas personas muy diferentes. Y también aumenta las posibilidades de cruzarnos con bots y personas con las que es difícil relacionarse. Por eso cada integrante de la comunidad tiene herramientas de moderación para cuidarse y cuidar al resto.

Estos son los lugares donde puedes encontrar las herramientas de moderación de nuestros servicios sociales:  

[FEDisroot](https://howto.disroot.org/es/tutorials/social/fedisroot/interacting#opciones-de-interaccion)  

[D·Scribe](https://howto.disroot.org/es/tutorials/social/dscribe/communities#moderacion)

[Movim](https://howto.disroot.org/es/tutorials/chat/webchat#moderacion)  
  

Hay dos niveles de moderación.

A cada persona usuaria corresponde la responsabilidad de bloquear y/o silenciar las cuentas con las que no desee interactuar. Si la cuenta es de una instancia externa, puede además enviar un reporte a los administradores de la misma.

Reportar a otra persona de nuestra misma instancia es un recurso que debe utilizarse cuando alguien está violando alguna de nuestras **Condiciones de Servicio**, estas pautas básicas mencionadas arriba o cuando las herramientas no son suficientemente efectivas en algún caso particular.

Los administradores pueden intervenir en cualquier momento si un caso lo amerita y tomar las medidas pertinentes.

Respetar y cuidar nuestra comunidad es una responsabilidad colectiva de todos sus miembros.


<a id="cambios"></a>
## 14. Cambios en estas Condiciones
Nos reservamos el derecho de modificar estas condiciones en cualquier momento. Valoramos mucho la transparencia y hacemos lo mejor para asegurarnos que las personas usuarias sean notificadas tan pronto como sea posible acerca de cualquier cambio significativo, via correo electrónico, redes sociales y/o en nuestro [blog](https://disroot.org/es/blog). Los cambios menores serán publicados seguramente solo en nuestro blog.
<br>
Puedes seguir el historial de cambios en nuestro repositorio Git, [**aquí**](https://git.disroot.org/Disroot/Disroot-ToS/commits/branch/main)o el [**registro de cambios**](https://git.disroot.org/Disroot/Disroot-ToS/src/branch/main/changelog)


[Volver al inicio](#indice)
