---
title: 'Nutzungsbedingungen'
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: tos
text_align: left
---

<br>
**v1.2.2 - Mai 2024**


## Über dieses Dokument

Dieses Dokument wurde im Original in Englisch verfasst. Die englische Version ist die einzige Version des Dokuments, für die **Stichting Disroot.org** haftbar gemacht werden kann.<br>
Jede Übersetzung der **Nutzungsbedingungen**, so wie die vorliegende, ist eine Bemühung der Community, die Informationen in anderen Sprachen zugänglich zu machen und sollte als solche verstanden werden, mit keinem anderen Wert als der reinen Information.

## Annahme der Bedingungen
Durch die Nutzung eines beliebigen, von **Disroot.org** bereitgestellten Angebots stimmst Du den [**Nutzungsbedingungen**](https://disroot.org/en/tos) zu. Daher solltest Du diese Bedingungen zunächst lesen, bevor Du unsere Plattform nutzt.

## Community Hinweis
**Disroot** ist eine Gemeinschaft und unsere Plattform ist Teil des "Freie und quelloffene Software"-Ökosystems (FOSS). Wir alle teilen Serverressourcen, arbeiten zusammen und nutzen gemeinsam die **Disroot.org**-Dienste. Es ist sehr wichtig, dass jede und jeder von uns das versteht und sich gleichermaßen dafür verantwortlich fühlt. Halte Dich daher bitte an die folgenden Richtlinien, dann werden wir alle miteinander klarkommen.

<a id="table-of-contents"></a>
# Inhaltsverzeichnis

## [Nutzungsbedingungen](#terms-of-services)<br>
[1. Wer kann unser Angebot nutzen?](#who)<br>
[2. Speicherplatzbeschränkungen auf der Festplatte](#space-limits)<br>
[3. Account-Verfall](#expiration)<br>
[4. Passwort](#password)<br>
[5. SPAM und Viren](#spam)<br>
[6. Copyright-Material](#copyright)<br>
[7. Nicht erlaubte Aktivitäten](#activities)<br>
[8. Nutzen des Disroot-Angebots für kommerzielle Aktivitäten](#commercial-activities)<br>
[9. Eigentum von und Verantwortlichkeit für Inhalte](#ownership)<br>
[10. Juristische Verantwortlichkeit](#legal)<br>
[11. Gesetze](#laws)<br>
[12. Account-Löschung](#account-termination)<br>
[13. Keine Garantie](#warranty)<br>

## [Grundlegende Leitlinien der Community für die gemeinsame Koexistenz](#community-guidelines)<br>
[1. Respektiere Andere, wenn Du respektiert werden möchtest](#respect)<br>
[2. Unterstütze keinen Missbrauch!](#abuse)<br>
[3. Keine Unterstützung von Intoleranz und Gewalt](#violence)<br>
[4. Kümmere dich um unser Netzwerk](#our-space)<br>

## [Änderungen dieser Nutzungsbedingungen](#changes)

<br>
---

<a id="terms-of-services"></a>


# Nutzungsbedingungen

<a id="who"></a>
## 1. Wer kann unser Angebot nutzen?
Jede Person, die mindestens 16 Jahre alt ist, kann unser Angebot nutzen.

<a id="space-limits"></a>
## 2. Speicherplatzbeschränkungen auf der Festplatte
**Ressourcen sind knapp**. Sei Dir bewusst, dass das Aufheben von unnötigen Dateien und E-Mails möglicherweise Serverplatz einschränkt, was dazu führen kann, dass weniger Accounts für Andere bereit stehen.
<br>**Nicht zumüllen!**

<a id="expiration"></a>
## 3. Account-Verfall
Aus Sicherheitsgründen löscht **Disroot.org** keine ungenutzten Accounts. Das kann sich in Zukunft eventuell ändern, wenn wir gezwungen sind, Speicherplatz für andere Nutzer zu schaffen. Solche Änderungen in unseren Bedingungen werden ausreichend im Voraus über alle Kommunikationskanäle bekanntgegeben.<br>Wir würden es jedoch vorziehen, wenn unsere Nutzer Verantwortung für ihren Account und dessen Einfluss auf die **Disroot**-Ressourcen übernehmen und ihren [**Account löschen**](https://user.disroot.org), wenn sie unsere Angebote nicht länger nutzen wollen, oder ungenutzte bzw. unnötige Dateien löschen.

<a id="password"></a>
## 4. Passwort
Wir überwachen niemanden, der die Aktivierung eines **Disroot**-Accounts beantragt. Wenn Du Deinen Account erstellst, kannst Du entweder Sicherheitsfragen und/oder das Hinzufügen einer zweiten E-Mailadresse auswählen. Beide Optionen werden es Dir ermöglichen, Dein Passwort zurückzusetzen, wenn Du es verlierst oder vergisst.<br> - Wenn Du keine zweite E-Mailadresse angibst oder die Antworten auf die Sicherheitsfragen vergisst, sind wir nicht in der Lage, Dir die notwendigen Daten zuzusenden, um wieder Zugriff auf Deinen Account zu erhalten. Denke auch daran, Dein Passwort regelmäßig zu ändern: Das kannst Du machen, indem Du Dich in den [User Self-Service](https://user.disroot.org) einloggst und auf 'Change password' bzw. 'Passwort ändern' klickst.<br>

**! Warnung !** Verlorene Passwörter führen zu einem nicht wiederherstellbaren Verlust Deiner Clouddaten. Um Dich davor zu schützen, kannst Du die Passwort-Wiederherstellung in Deinen persönlichen Cloud-Einstellungen aktivieren.

<a id="spam"></a>
## 5. SPAM und Viren
Um zu verhindern, dass unsere Server auf Sperrlisten landen:
- Wir untersagen die Nutzung von **Disroot**-E-Mailaccounts zum Versenden von SPAM. Jeder diesbezüglich festgestellte Account wird ohne weitere Mitteilung deaktiviert.
- Wenn Du eine größere Menge an SPAM oder verdächtigen E-Mails bekommst, sag uns bitte auf [**support@disroot.org**](mailto:support@disroot.org) Bescheid, damit wir uns das näher ansehen können.

<a id="copyright"></a>
## 6. Copyright-Material
Eines der Hauptziele von **Disroot.org** ist es, Freie und quelloffene Software (FOSS) und alle Arten von Copyleft-Inhalten zu fördern. Nimm auch Du dieses Ziel auf und unterstütze Künstler und Projekte, die Ihre Arbeit unter diesen Lizenzen veröffentlichen.
- Das Hochladen und (vor allem) das öffentliche Teilen von Copyright-Material auf **Disroot** kann sowohl andere Nutzer und **Disroot.org** in ernsthafte juristische Schwierigkeiten bringen als auch das ganze Projekt und die Existenz unserer Server (und aller damit verbundenen Angebote) gefährden, also **lass es einfach**.

<a id="activities"></a>
## 7. Nicht erlaubte Aktivitäten
Beteilige Dich nicht an den folgenden Handlungen, während Du die durch **Disroot.org** bereitgestellten Angebote nutzt:

<ol class=disc>
<li> <b>Belästigung und Beschimpfung anderer</b> durch die Beteiligung an Drohungen, Stalking oder dem Senden von SPAM.</li>
<li> <b>Beiträge zur Diskriminierung, Belästigung oder Schaden eines Individuums oder einer Gruppe.</b> Das umfasst auch das Verbreiten von Hass und Fanatismus durch Rassismus, Ethnophobie, Antisemitismus, Sexismus, Homophobie und andere Arten diskriminierenden Verhaltens.</li>
<li> <b>Beteiligung am Missbrauch anderer</b> durch die Verbreitung von Material, dessen Herstellungsprozess Gewalt oder sexuelle Übergriffe gegen Personen oder Tiere beinhaltete</li>
<li> <b>Imitation oder Darstellung einer anderen Person</b> auf eine verwirrende oder täuschende Weise, es sei denn, der Account ist deutlich als Parodie gekennzeichnet.</li>
<li> <b>Doxing</b>: die Veröffentlichung von persönlichen Daten einer anderen Person ist nicht erlaubt, denn:<br> (I) wir können weder die Absichten noch die Konsequenzen für die bloßgestellte Person abschätzen und (II) es bedeutet ein zu großes juristisches Risiko für <b>Disroot.org</b>. </li>
<li> <b>Missbrauch der Dienste</b> durch die Verbreitung von Viren oder Malware, Beteiligung an Denial-of-Service-Attacken oder den Versuch, sich unauthorisierten Zugriff auf irgendein Computersystem, dieses eingeschlossen, zu verschaffen.</li>
<li> <b>Verbotene Aktivitäten</b> nach nationalem oder internationalem Recht.</li>
</ol>

<a id="commercial-activities"></a>
## 8. Nutzen des Disroot-Angebots für kommerzielle Aktivitäten
**Disroot** ist eine Non-Profit-Organisation, die Dienste für Individuen auf einer "zahle soviel Du willst"-Basis anbietet. Aufgrund dieser Struktur verstehen wir die Nutzung der **Disroot**-Dienste für kommerzielle Zwecke als Missbrauch des Angebots und werden sie als solche behandeln.
Diese kommerziellen Aktivitäten oder Zwecke beinhalten (nicht ausschließlich):
<ol class=disc>
<li>Handeln jedes <b>Disroot</b>-Angebots mit einer dritten Partei.<br>Das ist untersagt.</li>
<li>Nutzen der E-Mail für Zwecke wie "no-reply"-Accounts für ein Geschäft.<br>Wenn wir eine solche Aktivität feststellen, wird der Account ohne Warnung blockiert.</li>
<li>Versenden von Massen-E-Mails, zum Beispiel aber nicht ausschließlich für Vertrieb oder Werbung zu geschäftlichen Zwecken.<br>
Der Account wird als Spam behandelt und bei Feststellung blockiert ohne vorherige Mitteilung.</li>
<li>Accounts die primär zum erzielen von finanziellen Gewinn genutzt werden, zum Beispiel aber nicht ausschließlich durch Handel oder zur Verkaufsverwaltung, werden nicht toleriert.<br> Solche Accounts werden bei Feststellung geschlossen.</li>
</ol>
<br>
So sehr wir es auch ablehnen <b>Disroot</b> für kommerzielle Zwecke zu nutzen, ist uns aber auch bewusst, dass manche finanziellen Aktivitäten Teil Deines persönlichen Lebens, Hobbys und Leidenschaften sein können.
<br>
Als Richtlinie, solltest du dich mit uns in Verbindung setzen, wenn eine kommerzielle Nutzung die private Nutzung übersteigt oder wenn du irgendwelche Zweifel und Fragen über den Umfang des Begriffs "kommerzielle Aktivitäten" hast.
<br>
Die Nutzung des <b>Disroot</b>-Angebots für andere kommerzielle Aktivitäten wird fallbezogen untersucht und die Entscheidung über eine Schließung des entsprechenden Accounts hängt ab von der Kommunikation mit dem Accountinhaber und der Art der fraglichen Aktivitäten.
<br>
Wenn Du Dir nicht sicher bist oder Fragen hast über die Interpretation des Begriffs "Kommerzielle Aktivitäten", kontaktiere uns bitte.

<a id="ownership"></a>
## 9. Eigentum von und Verantwortlichkeit für Inhalte
**Disroot.org** ist nicht verantwortlich für jegliche Inhalte, die auf einem unserer Dienste veröffentlicht werden. Du bist verantwortlich für Deine Nutzung der **Disroot**-Dienste, für jeden Inhalt, den Du bereitstellst und alle Konsequenzen daraus. Allerdings behalten wir uns, um unsere Existenz zu schützen, das Recht vor, jeden Inhalt zu löschen, wenn er gegen geltendes Recht verstößt.

<a id="legal"></a>
## 10. Juristische Verantwortlichkeit
Wie bei allem anderen, was Du tust, sei Dir bewusst, dass **Disroot.org** weder verantwortlich ist für das, was Du schreibst, noch für die Absicherung Deiner Privatsphäre. Wir laden Dich daher ein, das beste aus den existierenden Werkzeugen zu machen, um Deine Rechte zu schützen.<br>
Um mehr darüber zu erfahren, wie Deine Daten gespeichert und genutzt werden, sieh Dir unsere [**Datenschutzerklärung**](https://disroot.org/de/privacy_policy) an.

<a id="laws"></a>
## 11. Gesetze
**Disroot.org** wird in den **Niederlanden** gehostet und unterliegt daher **niederländischen** Gesetzen und Rechtssprechung.

<a id="termination"></a>
## 12. Account-Löschung
**Disroot.org** kann Deinen Zugang zu jeder Zeit unter den folgenden Bedingungen beenden:
<ol class=disc>
<li>Der Account wurde beim Versenden von SPAM (übermäßige Mengen unaufgeforderter E-Mails) identifiziert.</li>
<li>Der Account war an einer oder mehrerer der oben genannten verbotenen Aktivitäten beteiligt.</li>
</ol>
Die genannten Bedingungen können sich jederzeit ändern.

<a id="warranty"></a>
## 13. Keine Garantie
Du verstehst und bist einverstanden, dass **Disroot.org** Dir in erster Linie internetbasierte Dienste zur Verfügung stellt. Die Dienste werden angeboten wie sie sind, abhängig von der Verfügbarkeit und ohne Verbindlichkeit oder Haftung. So wenig wir auch nur einen einzigen unserer Nutzer enttäuschen wollen, können wir doch keine Garantie für die Zuverlässigkeit, Erreichbarkeit oder Qualität unserer Dienste geben.<br> Du bist einverstanden, dass die Nutzung unserer Dienste auf Dein alleiniges und ausschlie&szlig;liches Risiko geschieht.

<br>

<a id="community-guidelines"></a>
# Grundlegende Leitlinien der Community für die gemeinsame Koexistenz

**Disroot** ist eine Gemeinschaft, d.h. eine Gruppe von Menschen, die etwas verbindet. Und die größte Gemeinsamkeit neben der Tatsache, dass wir Nutzer der Plattform sind, besteht darin, dass wir alle unterschiedlich sind und daher besondere Arten des Denkens, Fühlens und der Zusammengehörigkeit haben.  

Unsere Nutzungsbedingungen gelten allgemein für alle Dienste. Aber in den Diensten, die für die Interaktion zwischen Menschen gedacht sind, ist es außerdem notwendig, die Auslebung unserer Unterschiede unter Bedingungen des Respekts und der Sicherheit zu gewährleisten, so dass wir uns alle ohne Angst vor schlechter Behandlung jeglicher Art ausdrücken können.

Um dieses Ziel zu erreichen, haben wir eine Reihe von Leitlinien für die Koexistenz aufgestellt. Diese Bedingungen sind nicht als perfekt und allumfassend gedacht, so dass sie falls nötig angepasst werden.
Die  Community wird dann darüber informiert.

Wenn du der Meinung bist, dass eine dieser Bedingungen deinen Prinzipien widerspricht, nicht zu dir passt oder deine Lebensweise in irgendeiner Weise einschränkt, kannst du dich entscheiden, unsere Instanzen nicht zu nutzen bzw. eine andere zu suchen, in der du dich weniger eingeschränkt fühlst.

<a id="respect"></a>
## 1. Respektiere Andere, wenn Du respektiert werden möchtest
Beleidigungen, Kommentare und abwertende Äußerungen gegenüber einer anderen Person oder Gruppe aufgrund ihres Geschlechts, ihrem sozialen Status, Ethnie, körperlichen, geistigen oder ideologischen Verfassung gelten als **gewalttätiges Verhalten** und werden nicht toleriert. Zu dieser Kategorie gehören auch **Drohungen** und **Belästigungen**.

<a id="abuse"></a>
## 2. Unterstütze keinen Missbrauch!

Die Verbreitung von Material, dessen Produktionsprozess zu Gewalt, sexueller Aggression und Ausbeutung von Erwachsenen oder Minderjährigen geführt hat, ist in unserer Gemeinschaft nicht zulässig.

<a id="violence"></a>
## 3. Keine Unterstützung von Intoleranz und Gewalt

Faschismus, Nationalsozialismus und andere politische oder religiöse Äußerungen, die Gewalt gegen Gruppen von Menschen fördern, haben bei uns keinen Platz.  

Sollte ein Mitglied der Community eine der oben genannten Richtlinien nicht respektieren oder absichtlich verletzen, kann sein Konto ohne Vorankündigung und sofort gesperrt oder sogar gelöscht werden.  

<a id="our-space"></a>
## 4. Kümmere dich um unser Netzwerk
Die offene und vielfältige Natur des Fediverse bietet vielfältige Möglichkeiten, mit vielen verschiedenen Menschen zu interagieren. Das erhöht aber auch die Wahrscheinlichkeit, dass man auf Bots und Leute trifft, mit denen man nicht gut auskommt. Deshalb verfügt jedes Mitglied der Gemeinschaft über Moderationswerkzeuge, um auf sich und andere aufzupassen.  

Hier findest du Moderationswerkzeuge für unsere sozialen Dienste:  

[FEDisroot](https://howto.disroot.org/de/tutorials/social/fedisroot/interacting#interacting-options)  

[D·Scribe](https://howto.disroot.org/de/tutorials/social/dscribe/communities#moderation)

[Movim](https://howto.disroot.org/de/tutorials/chat/webchat#moderation)  
  

Es gibt zwei Stufen der Moderation.

Jeder Nutzer ist selbst dafür verantwortlich, Konten, mit denen er nicht interagieren möchte, zu sperren und/oder stumm zu schalten. Wenn das Konto von einer externen Instanz stammt, können Sie auch einen Hinweis an die Administratoren dieser Instanz senden.   

Das Melden eines anderen Nutzers der selben Instanz ist ein Instrument, das genutzt werden sollte, wenn jemand gegen die **Nutzungsbedingungen**, die oben erwähnten grundlegenden Leitlinien verstößt oder wenn die oben genannten Werkzeuge in einem bestimmten Fall nicht effektiv genug sind.  

Die Administratoren können jederzeit eingreifen, wenn ein Fall dies rechtfertigt, und entsprechende Maßnahmen ergreifen.

Respekt und Sorgsamkeit für unsere Community ist eine kollektive Verantwortlichkeit aller Mitglieder.

<br>
<a id="changes"></a>
## 14. Änderungen dieser Nutzungsbedingungen
Wir behalten uns das Recht vor, diese Bedingungen jederzeit zu ändern. Wir schätzen Transparenz sehr und tun daher unser Bestes um sicherzustellen, dass unsere Nutzer so früh wie möglich per E-Mail, über Soziale Netzwerke und/oder auf unserem [Blog](https://disroot.org/de/blog) über wichtige Änderungen informiert werden. Über kleinere Änderungen werden wir vermutlich nur in unserem Blog informieren.

Du kannst die vergangenen Änderungen dieses Dokuments auf unserem [**Git-Repository**](https://git.disroot.org/Disroot/Disroot-ToS/commits/branch/main) oder im [**Änderungsprotokoll**](https://git.disroot.org/Disroot/Disroot-ToS/src/branch/main/changelog) verfolgen.


[Zurück zum Anfang](#table-of-contents)
