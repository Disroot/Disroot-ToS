---
title: 'Term Of Services'
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: tos
text_align: left
---

<br>
**v1.2.2 - May 2024**


## About this document
This document has been originally written in English and is the only version for which **Stichting Disroot.org** can be held accountable.<br>
Any translation of this **Terms of Services** is a community effort to make the information accessible in other languages and should be taken as such, with no other value than merely informative.

## Accepting these Terms
By using any of the services provided by **Disroot.org** you agree to be bound by our Terms of Services. So, you should start reading these terms before first using our platform.

## Community Notice
**Disroot** is a community and our platform a part of the Free Software and Open Source ecosystem. We all share servers resources, collaborate and use **Disroot.org** services together. It is very important that each and everyone of us understands it and feels equally responsible for it. So, please keep to the following guidelines so we all get along.

---

<a id="table-of-contents"></a>
# Table of Contents

## [Terms of Services](#terms-of-services)<br>
[1. Who can use our services?](#who)<br>
[2. Space limits on disk](#space-limits)<br>
[3. Account expiration](#expiration)<br>
[4. Password](#password)<br>
[5. SPAM and Viruses](#spam)<br>
[6. Copyright material](#copyright)<br>
[7. Activities not allowed](#activities)<br>
[8. Using Disroot services for commercial](#commercial-activities)<br>
[9. Ownership of and responsibility for content](#ownership)<br>
[10. Legal responsibilities](#legal)<br>
[11. Laws](#laws)<br>
[12. Account termination](#account-termination)<br>
[13. No Warranty](#warranty)<br>

## [Community's Basic Coexistence Guidelines](#community-guidelines)<br>
[1. Respect if we want to be respected](#respect)<br>
[2. Do not contribute to abuse](#abuse)<br>
[3. Do not promote intolerance and violence](#violence)<br>
[4. Taking care of our space](#our-space)<br>

## [Changes on these Terms](#changes)

<br>
---

<a id="terms-of-services"></a>
# Terms of Services

<a id="who"></a>
## 1. Who can use our services?
Any person who is at least 16 years of age can use the Services.

<a id="space-limits"></a>
## 2. Space limits on disk
**Resources are scarce**. Have in mind that keeping unnecessary files and emails might limit server space, which can lead to less accounts served to others.<br>**Don't litter!**

<a id="expiration"></a>
## 3. Account expiration
For safety reasons, **Disroot.org** does not expire unused accounts. This might be a subject to change if we are forced to save up disk space for other users. Such changes in our terms will be announced via all communication channels well in advance.<br>We would rather appreciate people taking responsibility for their account and its impact on **Disroot** resources, and rather [**delete their account**](https://user.disroot.org) when they decide not to use our services any longer, or purge unused files.

<a id="password"></a>
## 4. Password
We never keep track of anybody who requests the activation of a **Disroot** account. When creating your account, you may either choose security questions and/or adding a secondary email, both of which will allow you to recover your password in case you lose/forget it.<br>
- If you don't set a secondary email or forget the question or the answers to the questions, we won't be able to re-send you the data to access your account again. Remember as well to periodically change your password: you can do it by login into the [User Self-Service](https://user.disroot.org) and clicking the 'Change password' button.<br>

**! WARNING !** Lost passwords will lead to un-recoverable cloud file loss. To protect yourself from it, you can enable password recovery feature in your personal Cloud settings.

<a id="spam"></a>
## 5. SPAM and Viruses
To avoid having our servers included in blacklists:
- We do not allow the use of Disroot email accounts to send spam. Any account caught doing so will be disabled without further notice.

- If you happen to receive great amount of SPAM or any suspicious emails please let us know at support@disroot.org so we can investigate it further.

<a id="copyright"></a>
## 6. Copyright material
One of the main aims of **Disroot** is to promote Free Libre and Open Source software and all kind of copyleft content. Embrace it and support artists and projects that release their work under those licences.
- Uploading and (most of all) publicly sharing copyrighted material on **Disroot** could put other users and **Disroot.org** in serious legal troubles as well as endanger the whole project and the existence of our servers (and all services connected to it) so **just don't do it**.

<a id="activities"></a>
## 7. Activities not allowed
Do not engage in the following activities through the services provided by **Disroot.org**:

<ol class=disc>
<li> <b>Harassing and abusing others</b> by engaging in threats, stalking, or sending spam.</li>
<li> <b>Contributing to the discrimination, harassment or harm against any individual or group.</b> That includes the spread of hate and bigotry through racism, ethnophobia, antisemitism, sexism, homophobia and any other forms of discriminatory behavior.</li>
<li> <b>Contributing to the abuse of others</b> by distributing material where the production process created violence or sexual assault against persons or animals.</li>
<li> <b>Impersonating or portraying another person</b> in a confusing or deceptive manner unless the account is clearly classified as a parody.</li>
<li> <b>Doxing</b>: the publication of other people's personal data is not allowed because:<br> (I) we cannot assess either the purposes or what consequences it will have for the exposed people, and (II), it implies too big a legal risk for <b>Disroot.org</b>. </li>
<li> <b>Misuse of services</b> by distributing viruses or malware, engaging in a denial of service attack, or attempting to gain unauthorized access to any computer system, including this one.</li>
<li> <b>Activities not permitted</b> by national or international law.</li>
</ol>

<a id="commercial-activities"></a>
## 8. Using Disroot services for commercial activities
**Disroot** is a non-profit organization providing services for individuals on "pay as you wish" basis. Because of this structure we see using **Disroot** services for commercial purposes as an abuse of the service and it will be treated as such.
These commercial activities or purposes include, but are not limited to:
<ol class=disc>
<li>Trading any <b>Disroot</b> services with a third party.<br>This is prohibited.</li>
<li>Using email for purposes such as "no-reply" type of accounts for a business.<br>If such activity is detected, the account will be blocked without a warning.</li>
<li>Sending bulk emails, including but not limited to marketing and advertising, for business purposes.<br>
The account will be treated as spam and blocked upon discovery without any prior notice.</li>
<li>Accounts primarily used for financial gain, including but not limited to trading or managing sales, are not tolerated.<br>Those accounts will be subject to termination upon inquiry.</li>
</ol>
<br>
As much as we discourage using <b>Disroot</b> for commercial activities, we also understand that some financial activities can be part of your personal life, hobbies and passions.
<br>
As a rule of thumb, you should contact us, if your commercial use exceeds your personal use, or if you have any doubts and questions about the scope of the term "commercial activities".
<br>
Using <b>Disroot</b> services for other commercial activity will be examined per case and the decision on terminating such accounts will be based upon communication with the account holder and the type of the activities in question.
<br>
If you have any doubts or questions about the scope of the term "commercial activities", please contact us.

<a id="ownership"></a>
## 9. Ownership of and responsibility for content
**Disroot.org** is not responsible for any content published on any of our services. You are responsible for your use of the **Disroot**'s services, for any content you provide and for any consequences thereof. However, to secure our existence, we reserve the right to remove any content if found in violation with applicable law.

<a id="legal"></a>
## 10. Legal responsibilities
As for anything else you do, be aware that **Disroot.org** is not responsible for what you write, nor for the safeguard of your own privacy. We therefore invite you to make the most of all the existing tools to defend your rights.<br>
To learn more about how your information is being stored and used, please refer to our **Privacy Statement**.

<a id="laws"></a>
## 11. Laws
**Disroot.org** is hosted in the **Netherlands** and therefore is subject to **Dutch** laws and jurisdiction.

<a id="termination"></a>
## 12. Account termination
**Disroot.org** may terminate your service at any time under the following conditions:
<ol class=disc>
<li>The account has been found to be sending SPAM (excessive amounts of unsolicited email).</li>
<li>The account has engaged in one or more of the banned activities listed above.</li>
</ol>
The above conditions are subject to change at any time.

<a id="warranty"></a>
## 13. No Warranty
You understand and agree that **Disroot.org** provides you primarily with internet-based services. Therefore, services are offered as is, subject to availability and without liability to you. As much as we would not like to let any of our users down, we just can  not give any warranty as to the reliability, accessibility, or quality of our services.<br> You agree that the use of our services is at your sole and exclusive risk.
<br>

<a id="community-guidelines"></a>
# Community's Basic Coexistence Guidelines
**Disroot** is a community, that is, a group of people who share things in common. And the most common thing we share, besides being users of the platform, is that we are all different and therefore have particular ways of thinking, feeling and bonding.  

Our terms of use govern in a general way all services. But in those that are designed for people to interact, it is also necessary to guarantee the exercise of our differences in conditions of respect and security so that we can all express ourselves without fear of mistreatment of any kind.

To try to achieve this, we have established a series of basic conditions of coexistence. These conditions are not intended to be perfect and monolithic, so they may change for the better. When necessary, the community will be duly informed.  

In case you consider that any of these conditions contradicts or does not fit your principles or limits in any way your way of being, you can choose not to use our instances or look for another one where you feel less conditioned.

<a id="respect"></a>
## 1. Respect if we want to be respected
Insults, comments and disqualifying speech to another person or group because of their gender, social, racial, physical, mental or ideological condition are considered **violent behavior** and are not tolerated. This category also includes **threats** and **harassment**.

<a id="abuse"></a>
## 2. Do not contribute to abuse
The distribution of material whose production process has generated violence, sexual aggression and exploitation of adults or minors is not admissible in our community.

<a id="violence"></a>
## 3. Do not promote intolerance and violence
Fascism, Nazism and any other political or religious expression that promotes violence against groups of people has no place in this instance.  

In case that any member of the community does not respect or intentionally violates any of the above guidelines, their account may be blocked or even deleted without notice and immediately.  

<a id="our-space"></a>
## 4. Taking care of our space
The open and diverse nature of the Fediverse enables many ways to interact with many very different people. And it also increases the chances of coming across bots and people who are difficult to interact with. That's why every member of the community has moderation tools to take care of themselves and others.  

These are the places where you can find moderation tools for our social services:  

[FEDisroot](https://howto.disroot.org/en/tutorials/social/fedisroot/interacting#interacting-options)  

[D·Scribe](https://howto.disroot.org/en/tutorials/social/dscribe/communities#moderation)

[Movim](https://howto.disroot.org/en/tutorials/chat/webchat#moderation)  
  

There are two levels of moderation.

Each user is responsible for blocking and/or muting accounts that they do not wish to interact with. If the account is from an external instance, you can also send a report to the admins of that instance.  

Reporting another user of the same instance is a resource to be used when someone is violating any of the **Terms of use**, these basic guidelines mentioned above or when the tools are not effective enough in a particular case.  

The admins can intervene at any time if a case warrants it and take the appropriate measures.

Respect and care for our community is a collective responsibility of all its members.

<br>
<a id="changes"></a>
## Changes on these Terms
We reserve the right to modify these conditions at any time. We highly evaluate transparency and do our best to make sure users are notified as soon as possible about any major changes via email, social networks and/or on our [blog](https://disroot.org/en/blog). Minor changes will most likely only be published on our blog.

You can follow the history of changes on this document on our git repository [**here**](https://git.disroot.org/Disroot/Disroot-ToS/commits/branch/main) or the [**changelog**](https://git.disroot.org/Disroot/Disroot-ToS/src/branch/main/changelog)


[Back to top](#table-of-contents)
