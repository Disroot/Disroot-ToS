---
title: 'Termini di servizio'
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: tos
text_align: left
---

<br>
**v1.2.2 - Maggio 2024**


## Informazioni sul documento
Questo documento è stato originariamente scritto in inglese ed è l'unica versione per cui **Stichting Disroot.org** può essere ritenuto responsabile.<br>
Qualsiasi traduzione di questi **Termini di servizio** è uno sforzo della comunità per rendere le informazioni accessibili in altre lingue e deve essere considerata come tale, senza altro valore che quello meramente informativo.

## Accettazione dei Termini
Utilizzando uno qualsiasi dei servizi forniti da **Disroot.org** si accetta di essere vincolati dai nostri Termini di servizio. Pertanto, è necessario iniziare a leggere questo documento prima di utilizzare per la prima volta la nostra piattaforma.

## Avviso alla comunità
**Disroot** è una comunità e la nostra piattaforma fa parte dell'ecosistema del Software Libero e Open Source. Tutti noi condividiamo le risorse dei server, collaboriamo e utilizziamo insieme i servizi di **Disroot.org**. È molto importante che ognuno di noi lo capisca e se ne senta ugualmente responsabile. Pertanto, vi preghiamo di attenervi alle seguenti linee guida, in modo da andare tutti d'accordo.

---

<a id="table-of-contents"></a>
# Sommario

## [Termini di servizio](#terms-of-services)<br>
[1. Chi può utilizzare i nostri servizi?](#who)<br>
[2. Limite di spazio sui dischi](#space-limits)<br>
[3. Scadenza dell'account](#expiration)<br>
[4. Password](#password)<br>
[5. SPAM e Virus](#spam)<br>
[6. Materiale protetto da copyright](#copyright)<br>
[7. Attività non permesse](#activities)<br>
[8. Utilizzo dei servizi Disroot per le attività commerciali](#commercial-activities)<br>
[9. Proprietà e responsabilità dei contenuti](#ownership)<br>
[10. Responsabilità legali](#legal)<br>
[11. Leggi](#laws)<br>
[12. Chiusura del conto](#account-termination)<br>
[13. Nessuna garanzia](#warranty)<br>

## [Linee guida comunitarie di base per la coesistenza](#community-guidelines)<br>
[1. Rispettare se vogliamo essere rispettati](#respect)<br>
[2. Non contribuire agli abusi](#abuse)<br>
[3. Non promuovere l'intolleranza e la violenza](#violence)<br>
[4. Prendersi cura del nostro spazio](#our-space)<br>

## [Cambiamenti dei termini di servizio](#changes)

<br>
---

<a id="terms-of-services"></a>
# Termini di servizio

<a id="who"></a>
## 1. Chi può utilizzare i nostri servizi?
Qualsiasi persona che abbia almeno 16 anni di età.

<a id="space-limits"></a>
## 2. Limite di spazio sui dischi
Le **risorse sono scarse**. Tenete presente che il mantenimento di file ed e-mail non necessari può limitare lo spazio sul server, il che può portare a una riduzione dei servizi proposti agli utenti.<br>

<a id="expiration"></a>
## 3. Scadenza dell'account
Per ragioni di sicurezza, **Disroot.org** non fa scadere gli account inutilizzati. Questo potrebbe essere soggetto a modifiche nel caso in cui fossimo costretti a risparmiare spazio sui dischi per altri utenti. Tali modifiche ai nostri termini di servizio saranno annunciate con largo anticipo attraverso tutti i canali di comunicazione. <br>Apprezzeremmo però che le persone si assumessero la responsabilità del loro account e del suo impatto sulle risorse di **Disroot**, e [**cancellassero il loro account**](https://user.disroot.org) quando decideranno di non utilizzare più i nostri servizi.

<a id="password"></a>
## 4. Password
Non teniamo traccia di chi richiede l'attivazione di un account **Disroot**. Durante la creazione del tuo account, puoi scegliere di impostare delle domande di sicurezza e/o aggiungere un'email secondaria, entrambe ti permetteranno di recuperare la password nel caso la dimentichi o la perdi.<br>
- Se non imposti un'email secondaria o dimentichi la domanda o le risposte alle domande, non saremo in grado di inviarti di nuovo i dati per accedere al tuo account. Ricorda anche di cambiare periodicamente la tua password: puoi farlo accedendo al [Servizio Self-Service per Utenti](https://user.disroot.org) e cliccando sul pulsante 'Cambia password'.<br>

**! ATTENZIONE !** Le password perse comporteranno la perdita irrecuperabile dei file nel cloud. Per proteggerti da ciò, puoi abilitare la funzione di recupero password nelle impostazioni personali del tuo Cloud.


<a id="spam"></a>
## 5. SPAM e Virus
Per evitare che i nostri server vengano inclusi nelle black list:
- Non consentiamo l'uso degli account email Disroot per inviare spam. Qualsiasi account sorpreso a farlo verrà disabilitato senza ulteriori avvisi.

- Se dovessi ricevere una grande quantità di SPAM o email sospette, ti preghiamo di farcelo sapere all'indirizzo support@disroot.org in modo che possiamo investigare ulteriormente.

<a id="copyright"></a>
## 6. Materiale protetto da copyright
Uno degli obiettivi principali di **Disroot** è promuovere il software libero e aperto e tutti i tipi di contenuti copyleft. Abbraccia questa filosofia e sostieni artisti e progetti che rilasciano il loro lavoro sotto queste licenze.
- Caricare e (soprattutto) condividere pubblicamente materiale protetto da copyright su **Disroot** potrebbe causare seri problemi legali agli altri utenti e a **Disroot.org**, nonché mettere in pericolo l'intero progetto e l'esistenza dei nostri server (e tutti i servizi ad esso collegati) quindi **semplicemente non farlo**.

<a id="activities"></a>
## 7. Attività non consentite
Non intraprendere le seguenti attività attraverso i servizi forniti da **Disroot.org**:

<ol class=disc>
<li> <b>Molestare e abusare degli altri</b> con minacce, stalking o invio di spam.</li>
<li> <b>Contribuire alla discriminazione, molestia o danni contro qualsiasi individuo o gruppo.</b> Questo include la diffusione di odio e bigottismo attraverso il razzismo, l'etnofobia, l'antisemitismo, il sessismo, l'omofobia e qualsiasi altra forma di comportamento discriminatorio.</li>
<li> <b>Contribuire all'abuso degli altri</b> distribuendo materiale in cui il processo produttivo ha creato violenza o aggressione sessuale contro persone o animali.</li>
<li> <b>Assumere o ritrarre un'altra persona</b> in modo confuso o ingannevole a meno che l'account non sia chiaramente classificato come una parodia.</li>
<li> <b>Doxing</b>: non è consentita la pubblicazione dei dati personali di altre persone perché: <br> (I) non possiamo valutare né le finalità né le conseguenze che avrà per le persone esposte, e (II) comporta un rischio legale troppo elevato per <b>Disroot.org</b>.</li>
<li> <b>Abuso dei servizi</b> distribuendo virus o malware, impegnandosi in attacchi DDoS o tentando di ottenere accesso non autorizzato a qualsiasi sistema informatico, incluso questo.</li>
<li> <b>Attività non consentite</b> dalla legge nazionale o internazionale.</li>
</ol>

<a id="commercial-activities"></a>
## 8. Utilizzo dei servizi Disroot per attività commerciali
**Disroot** è un'organizzazione no-profit che fornisce servizi per individui su base "paga quanto vuoi". A causa di questa struttura, riteniamo l'uso dei servizi **Disroot** per scopi commerciali un abuso del servizio e verrà trattato come tale.
Queste attività o scopi commerciali includono, ma non sono limitati a:
<ol class=disc>
<li>Scambiare qualsiasi servizio **Disroot** con un terzo.<br>Questo è vietato.</li>
<li>Utilizzare l'email per scopi come account di tipo "no-reply" per un'attività commerciale.<br>Se tale attività viene rilevata, l'account verrà bloccato senza preavviso.</li>
<li>Inviare email di massa, incluse ma non limitate a marketing e pubblicità, per scopi commerciali.<br>
L'account verrà trattato come spam e bloccato al momento del rilevamento senza alcun preavviso.</li>
<li>Account utilizzati principalmente per guadagni finanziari, incluse ma non limitate a negoziazioni o gestione di vendite, non sono tollerati.<br>Questi account saranno soggetti alla chiusura su richiesta.</li>
</ol>
<br>
Pur scoraggiando l'uso di **Disroot** per attività commerciali, comprendiamo anche che alcune attività finanziarie possono far parte della tua vita personale, hobby e passioni.
<br>
Come regola generale, dovresti contattarci se il tuo utilizzo commerciale supera il tuo utilizzo personale, o se hai dubbi e domande sullo scopo del termine "attività commerciali".
<br>
L'uso dei servizi **Disroot** per altre attività commerciali verrà esaminato caso per caso e la decisione sulla chiusura di tali account sarà basata sulla comunicazione con il titolare dell'account e sul tipo di attività in questione.
<br>
Se hai dubbi o domande sullo scopo del termine "attività commerciali", ti preghiamo di contattarci.

<a id="ownership"></a>
## 9. Proprietà e responsabilità dei contenuti
**Disroot.org** non è responsabile di alcun contenuto pubblicato su nessuno dei nostri servizi. Sei responsabile del tuo utilizzo dei servizi di **Disroot**, per tutti i contenuti che fornisci e per tutte le conseguenze derivanti da essi. Tuttavia, per garantire la nostra esistenza, ci riserviamo il diritto di rimuovere qualsiasi contenuto se trovato in violazione della legge applicabile.

<a id="legal"></a>
## 10. Responsabilità legali
Per qualsiasi altra cosa tu faccia, sii consapevole che **Disroot.org** non è responsabile di ciò che scrivi, né della salvaguardia della tua privacy. Ti invitiamo quindi a sfruttare al meglio tutti gli strumenti esistenti per difendere i tuoi diritti.<br>
Per saperne di più su come vengono memorizzate e utilizzate le tue informazioni, consulta la nostra **Informativa sulla Privacy**.

<a id="laws"></a>
## 11. Leggi
**Disroot.org** è ospitato nei **Paesi Bassi** e quindi è soggetto alle leggi e alla giurisdizione olandesi.

<a id="termination"></a>
## 12. Chiusura dell'account
**Disroot.org** può chiudere il tuo account in qualsiasi momento nelle seguenti situazioni:
<ol class=disc>
<li>L'account è stato trovato a inviare SPAM (quantità eccessive di email non richieste).</li>
<li>L'account ha partecipato a una o più delle attività vietate elencate sopra.</li>
</ol>
Le condizioni sopra elencate sono soggette a modifiche in qualsiasi momento.

<a id="warranty"></a>
## 13. Nessuna garanzia
Comprendi e accetti che **Disroot.org** ti fornisce principalmente servizi basati su internet. Di conseguenza, i servizi sono offerti così come sono, soggetti a disponibilità e senza responsabilità nei tuoi confronti. Per quanto non vogliamo deludere nessuno dei nostri utenti, non possiamo garantire in alcun modo l'affidabilità, l'accessibilità o la qualità dei nostri servizi.<br>Accetti che l'uso dei nostri servizi avvenga esclusivamente a tuo rischio.

<a id="community-guidelines"></a>
# Linee guida di base per la convivenza della comunità

**Disroot** è una comunità, ovvero un gruppo di persone che condividono cose in comune. E la cosa più comune che condividiamo, oltre ad essere utenti della piattaforma, è che siamo tutti diversi e quindi abbiamo modi particolari di pensare, sentire e relazionarci.

Le nostre condizioni d'uso regolano in generale tutti i servizi. Ma in quelli progettati per l'interazione tra persone, è anche necessario garantire l'esercizio delle nostre differenze in condizioni di rispetto e sicurezza in modo che tutti possiamo esprimerci senza paura di maltrattamenti di qualsiasi tipo.

Per cercare di raggiungere questo obiettivo, abbiamo stabilito una serie di condizioni di base per la convivenza. Queste condizioni non sono destinate ad essere perfette e monolitiche, quindi potrebbero cambiare per il meglio. Quando necessario, la comunità sarà adeguatamente informata.

Nel caso tu ritenga che una qualsiasi di queste condizioni contraddica o non si adatti ai tuoi principi o limiti in qualsiasi modo il tuo modo di essere, puoi scegliere di non utilizzare le nostre istanze o cercare un'altra dove ti senti meno condizionato.

<a id="respect"></a>
## 1. Rispetto se vogliamo essere rispettati
Gli insulti, i commenti e i discorsi denigratori rivolti a un'altra persona o gruppo a causa del loro genere, condizione sociale, razza, condizione fisica, mentale o ideologica sono considerati **comportamenti violenti** e non sono tollerati. Questa categoria include anche le **minacce** e il **molestare**.

<a id="abuse"></a>
## 2. Non contribuire all'abuso
La distribuzione di materiale il cui processo di produzione ha generato violenza, aggressione sessuale e sfruttamento di adulti o minori non è ammissibile nella nostra comunità.

<a id="violence"></a>
## 3. Non promuovere l'intolleranza e la violenza
Il fascismo, il nazismo e qualsiasi altra espressione politica o religiosa che promuove la violenza contro gruppi di persone non ha posto in questa istanza.

Nel caso in cui un membro della comunità non rispetti o violi intenzionalmente una qualsiasi delle linee guida sopra indicate, il suo account potrebbe essere bloccato o addirittura eliminato.

<a id="our-space"></a>
## 4. Prendersi cura del nostro spazio
La natura aperta e diversificata del Fediverse offre molte modalità di interazione con persone molto diverse. E aumenta anche le possibilità di incontrare bot e persone con cui è difficile interagire. Per questo motivo, ogni membro della comunità dispone di strumenti di moderazione per prendersi cura di se stessi e degli altri.

Questi sono i luoghi in cui puoi trovare gli strumenti di moderazione per i nostri servizi social:

[FEDisroot](https://howto.disroot.org/en/tutorials/social/fedisroot/interacting#interacting-options)  

[D·Scribe](https://howto.disroot.org/en/tutorials/social/dscribe/communities#moderation)

[Movim](https://howto.disroot.org/en/tutorials/chat/webchat#moderation)  
  

Esistono due livelli di moderazione.

Ogni utente è responsabile di bloccare e/o silenziare gli account con cui non desidera interagire. Se l'account proviene da un'istanza esterna, è anche possibile inviare una segnalazione agli amministratori di quella istanza.

Segnalare un altro utente della stessa istanza è una risorsa da utilizzare quando qualcuno sta violando uno qualsiasi dei **Termini d'uso**, queste linee guida di base menzionate sopra o quando gli strumenti non sono abbastanza efficaci in un caso particolare.

Gli amministratori possono intervenire in qualsiasi momento se un caso lo richiede e adottare le misure appropriate.

Il rispetto e la cura della nostra comunità sono una responsabilità collettiva di tutti i suoi membri.

<br>
<a id="changes"></a>
## Modifiche a questi Termini di servizio
Ci riserviamo il diritto di modificare queste condizioni in qualsiasi momento. Valutiamo molto la trasparenza e facciamo del nostro meglio per assicurarci che gli utenti siano informati il prima possibile su eventuali modifiche importanti tramite email, social network e/o sul nostro [blog](https://disroot.org/en/blog). Le modifiche minori probabilmente verranno pubblicate solo sul nostro blog.

Puoi seguire la cronologia delle modifiche su questo documento nel nostro repository git [**qui**](https://git.disroot.org/Disroot/Disroot-ToS/commits/branch/main) o nel [**changelog**](https://git.disroot.org/Disroot/Disroot-ToS/src/branch/main/changelog)

[Torna all'inizio](#table-of-contents)